﻿
public class GameEvents {
    public readonly static string BID_END = "BID_END";
    public readonly static string KITTY_DEPOSIT = "KITTY_DEPOSIT";
    public readonly static string KITTY_DEPOSITED = "KITTY_DEPOSITED";

    public readonly static string ROUND_DETAILS_UPDATE = "ROUND_DETAILS_UPDATE";
    public readonly static string PARTNER_SELECTED = "PARTNER_SELECTED";
    public readonly static string PARTNER_REQUEST = "PARTNER_REQUEST";
    public readonly static string NEW_TRICK = "NEW_TRICK";
    public readonly static string NEW_ROUND = "NEW_ROUND";
    public readonly static string PLAY_REQUEST = "PLAY_REQUEST";
    public readonly static string TRICK_UPDATE = "TRICK_UPDATE";
    public readonly static string HAND_UPDATE = "HAND_UPDATE";

}

