
public enum GameState {
    PRE_DEAL,
    DEAL,
    KITTY,
    PARTNER,
    PLAY,
    END
}