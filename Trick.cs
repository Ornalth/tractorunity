﻿
using System.Collections.Generic;

public class Trick {
    //Cards played by each player
    private List<Card>[] cards = new List<Card>[Constants.NUM_PLAYERS];
    //int winner;
    private int leader;
    private PlayType playType;

    public PlayType getPlayType() {
        return playType;
    }
    public List<Card> getPlay(int i) {
        return cards[i];
    }
    public void setPlay(int index, List<Card> cards) {
        if (isLead()) {
            leader = index;
        }
        //Logger::logMessage("WE SET THIS PLAYER " + std::to_string(index) + " " + std::to_string(cards.Count));
        this.cards[index] = new List<Card>(cards);
        // if (isDone()) {
        //     getWinner();
        // }
    }
    public void setPlayType(PlayType pt) {
        playType = pt;
    }
    public bool isLead() {
        for (int i = 0; i < cards.Length; i++) {
            List<Card> playersCards = cards[i];
            if (playersCards.Count > 0) {
                return false;
            }
        }
        return true;
    }
    public int getWinner() {
        int bestIndex = leader;
        //WE can ignore leader.
        PlayType bestPlayType = playType;

        for (int i = 1; i < Constants.NUM_PLAYERS; i++) {
            int index = (i + leader) % Constants.NUM_PLAYERS;
            List<Card> playedCards = cards[index];
            //Logger::logMessage("WHAT IS HAPPENING" + std::to_string(index));
            //Player didnt play yet.
            if (playedCards.Count == 0) {
                continue;
            }

            if (bestPlayType.isBigger(playedCards)) {
  //              Logger.logMessage($"THIS IS BIGGER {index} old {CardUtils.getCardListString(bestPlayType.getCardsPlayed())} new {CardUtils.getCardListString(playedCards)}");
                bestPlayType = new PlayType(playedCards, playType);
//                Logger.logMessage($"NEW PLAY TYPE {bestPlayType.ToString()}");
                bestIndex = index;
            }
        }
        

        return bestIndex;
    }
   
    public bool isDone() {
        for (int i = 0; i < cards.Length; i++) {
            List<Card> playersCards = cards[i];
            if (playersCards.Count == 0) {
                return false;
            }
        }
        return true;
    }
    public List<Card>[] getCards() {
        return cards;
    }
    public Trick(int lead) {
        for (int i = 0; i < Constants.NUM_PLAYERS; i++) {
            cards[i] = new List<Card>();
        }
        this.leader = lead;
    }

    public int getTotalPoints() {
        int sum = 0;

        foreach (List<Card> playerCards in cards) {
            sum += CardUtils.getTotalPoints(playerCards);
        }

        return sum;
    }
    public override string ToString() {
        string str = "";
        for (int i = 0; i < Constants.NUM_PLAYERS; i++) {
            str = str + "Player: " + i + ": ";
            foreach (Card cardsPlayed in cards[i]) {
                str = str + cardsPlayed.ToString() + " ";
            }
            if (i == leader) {
                str = str + " Lead ";
            }
            str += "\n";
        }
        if (playType!= null) {
            str += $" Playtype: {playType.ToString()} ";
        }
        str = str + "Value: " + getTotalPoints();
        return str;
    }
    public int getLeader() {
        return leader;
    }

    public List<int> getPlayersLeftToPlay() {
        List<int> indices = new List<int>();
        for (int i = 0; i < Constants.NUM_PLAYERS; i++) {
             List<Card> c = cards[i];
            if (c == null || c.Count == 0) {
                indices.Add(i);
            }
        }
        return indices;
    }
}
