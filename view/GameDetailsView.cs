using UnityEngine;
using System.Collections.Generic;
using TMPro;

public class GameDetailsView : MonoBehaviour {

    TextMeshPro  textBox;

	void Awake() {
		textBox = gameObject.GetComponentInChildren<TextMeshPro >();
	}

	public void updateGame(Game g) {
		string output = genString(g);
        textBox.text = output;
	}
    private string genString(Game g) {


		List<string> wat = new List<string>() {
			$"Trump Rank:{GlobalMembers.RANK_STRING_MAPPING[g.getTrumpRank()]}",

		};
		if (g.getState() == GameState.PRE_DEAL) {
			wat.Add($"Kitty Contained: {CardUtils.getCardListString(g.getKitty())}");
		} else if (g.getState() == GameState.DEAL) {
			wat.Add($"Largest Bid: {g.getLargestBid()}");
		} else if (g.getState() == GameState.KITTY) {
			wat.Add($"Select 8 cards to bury...");
		} else if (g.getState() == GameState.PARTNER) {
			wat.Add($"Select Partner...");
		} else if (g.getState() == GameState.PLAY) {
			wat.Add($"Trump Suit:{GlobalMembers.TRUMP_SUIT_STRING_MAPPING[g.getTrumpSuit()]}");
			wat.Add($"Partner Card:{g.getPartnerCard()}");
			wat.Add($"Nth Card:{g.getNthPartnerCardRequired()}");
			wat.Add($"Attacker Points:{g.getAttackerPoints()}");
			wat.Add($"Defender Points:{g.getDefenderPoints()}");
			wat.Add($"Last Trick:{g.getLastTrick()}");
		}
        return string.Join("\n", wat);
    }
}
