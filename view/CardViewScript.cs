using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class CardViewScript : MonoBehaviour {
	public interface CardViewController {
		void cardClicked(CardViewScript scr, Card c);
	}


	Card card;
	bool invisible = false;
	CardViewController handController = null;
	SpriteRenderer face;
	SpriteRenderer back;
	List<SpriteRenderer> allSprites = new List<SpriteRenderer>();
	bool selected = false;

	void Awake()
	{
		
		SpriteRenderer[] sprites = gameObject.GetComponentsInChildren<SpriteRenderer>();
		
		foreach (SpriteRenderer sprite in sprites)
		{
			if (sprite.name.Equals("Face"))
			{
				face = sprite;
				face.color = new Color(255,255,255);
			}
			else if (sprite.name.Equals("Back"))
			{
				back = sprite;
			}
			allSprites.Add(sprite);
		}
	}

	public void setSortingOrder(int id)
	{
		//this.transform.GetComponent<Renderer>().sortingLayerID = id;

		// MeshRenderer[] renders =  gameObject.GetComponentsInChildren<MeshRenderer> ();

		// foreach (MeshRenderer render in renders)
		// {
		// 	render.sortingLayerID = id;
		// 	render.sortingOrder = 50;
		// }

		float maxZ = 14f;
		face.sortingOrder = id+1;
		back.sortingOrder =id;
		Transform trans = this.transform;
		trans.localPosition = new Vector3(trans.localPosition.x, trans.localPosition.y, maxZ - id/2.0f);
		// Debug.Log($"HELO? {id}");
		// foreach (SpriteRenderer obj in allSprites)
		// {
		// 	obj.sortingLayerID = id;
		// }
	}


	// void hideCard()
	// {
	// 	ImageLibrary.setBaseHidden(baseRenderer);
	// 	Renderer[] renderers = GetComponentsInChildren<Renderer> ();
	// 	foreach (Renderer render in renderers)
	// 	{
	// 		render.enabled = false;
	// 	}
	// 	baseRenderer.enabled = true;
	// }

	public void setHidden(bool hidden) {
		invisible = hidden;
		if (hidden) {
			ViewHelper.hide(this.gameObject);
		}
		else {
			ViewHelper.show(this.gameObject);
		}
	}

	void OnMouseDown () 
	{
		if (handController != null)
			handController.cardClicked (this, card);
	}

	public void setCard(Card c) {
		if (c != card) {
			card = c;
			updateCard ();
		}
	}

	private void updateCard()
	{
		if (invisible) {
			return;
		}
		face.sprite = ImageLibrary.getCardImage(card);

		//TODO change sprite.
		//dizzyRenderer.sprite = ImageLibrary.getCardImage(card.getDizzy().getDizzy());
	}
	private void updateSelected() {
		if (invisible) {
			return;
		}
		Transform trans = this.transform;
		if (selected) {
			trans.localPosition = new Vector3(trans.localPosition.x, 0.5f, trans.localPosition.z);
		} else {
			trans.localPosition = new Vector3(trans.localPosition.x, 0f, trans.localPosition.z);
		}
	}

	public void setController(CardViewController h) {
		handController = h;
	}

	public void changeSelected() {
		selected = !selected;
		updateSelected();
	}

	public void setSelected(bool s) {
		if (selected != s) {
			selected = s;
			updateSelected();
		}
	}
	public bool isSelected() {
		return selected;
	}
}
