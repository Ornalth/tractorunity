using UnityEngine;
using System.Collections.Generic;

using TMPro;

public class MiscTextView : MonoBehaviour {

    TextMeshPro  textBox;

	void Awake() {
		textBox = gameObject.GetComponentInChildren<TextMeshPro >();
	}

  public void update() {
      textBox.text = "";
  }
	public void update(Card c, int nth) {
		string output = genString(c, nth);
        textBox.text = output;
	}
    private string genString(Card c, int nth) {
       if (c == null) {
           return "Selecting alone.";
       }
       return $"Selecting {nth} card of {c} for partner";
    }
}
