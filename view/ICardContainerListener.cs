﻿using UnityEngine;
using System.Collections;


public interface ICardContainerListener
{
	void cardClicked(CardContainerScript scr, Card c);
}

