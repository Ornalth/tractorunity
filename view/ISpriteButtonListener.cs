﻿using UnityEngine;
using System.Collections;


public interface ISpriteButtonListener
{
	void buttonClicked(SpriteButton scr);
}

