using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public static class ViewHelper {

	public static void hide(GameObject view) {
		view.SetActive (false);
	}
	
	public static void show(GameObject view) {
		view.SetActive (true);
	}
}
