﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainBoardVC : MonoBehaviour, LoggerListener, ModelSub, ISpriteButtonListener {

	Game model;

	GameObject handGameObject;
	HandViewScript handViewScript;

	GameObject[] boardGameObjects;
	HandViewScript[] boardGameViewScripts;

	SpriteButton passButtonScript;


	SpriteButton diamondsButtonScript;
	SpriteButton clubsButtonScript;
	SpriteButton heartsButtonScript;
	SpriteButton spadesButtonScript;
	SpriteButton noTrumpButtonScript;
	SpriteButton firstCardButtonScript;
	SpriteButton secondCardButtonScript;
	Dictionary<SpriteButton, Rank> rankDictMappingScripts = new Dictionary<SpriteButton,Rank>();

	int partnerNth = 1;
	Rank partnerRank = Rank.ACE;
	Suit partnerSuit = Suit.SPADES;

	Card partnerCard; 

	MiscTextView miscScript;
	GameDetailsView gameDetailsScript;
	PlayerDetailsView[] playerDetailsViewScripts = new PlayerDetailsView[5];

	public void logMessage(string s) {
		Debug.Log(s);
	}
	// Use this for initialization
	void Start () {
		initViews();
		initGame();
	}

	void initGame() {
		partnerCard = new Card(partnerRank, partnerSuit);
		Logger.subscribe(this);
		model = new Game();
		model.addSubscriber(this);
		model.newGame(Constants.NUM_PLAYERS);
	}

	void initViews() {
		handGameObject = Instantiate (Resources.Load ("Hand")) as GameObject;
		handGameObject.name = "Main Player hand";
		handGameObject.transform.parent = this.transform;
		handViewScript = handGameObject.GetComponent<HandViewScript>();
		handGameObject.transform.localPosition = new Vector3(0f, -12.5f, 0f);

		boardGameObjects = new GameObject[Constants.NUM_PLAYERS];
		boardGameViewScripts = new HandViewScript[Constants.NUM_PLAYERS];
		for (int i = 0; i < Constants.NUM_PLAYERS; i++) {
			boardGameObjects[i] = Instantiate (Resources.Load ("Hand")) as GameObject;
			boardGameObjects[i].transform.parent = this.transform;
			boardGameObjects[i].name = $"Player {i} board/hand";
			boardGameViewScripts[i] = boardGameObjects[i].GetComponent<HandViewScript>();
			boardGameObjects[i].transform.localPosition = new Vector3(0f, -11.5f + i * 5.5f, 0f);
		}

		initTextViews();
		initButtons();
	}

		//TODO
	private void initTextViews() {
		GameObject miscView = Instantiate(Resources.Load ("MiscView")) as GameObject;
		miscView.name = "miscview";
		miscView.transform.parent = this.transform;
		miscScript = miscView.GetComponent<MiscTextView>();
		miscView.transform.localPosition = new Vector3(5f,0f,0f);
		//TODO set location

		GameObject detailsView = Instantiate(Resources.Load ("GameDetailsView")) as GameObject;
		detailsView.name = "detailsView";
		detailsView.transform.parent = this.transform;
		gameDetailsScript = detailsView.GetComponent<GameDetailsView>();
		detailsView.transform.localPosition = new Vector3(-9.5f,12f,0f);

		for (int i = 0; i < Constants.NUM_PLAYERS; i++) {
			GameObject playerView = Instantiate(Resources.Load ("PlayerDetailsView")) as GameObject;
			playerView.name = $"playerdetailsView{i}";
			playerView.transform.parent = this.transform;
			playerDetailsViewScripts[i] = playerView.GetComponent<PlayerDetailsView>();
			playerView.transform.localPosition = new Vector3(-9.5f,-14f+i*3.75f,0f);
		}
	}

	SpriteButton createButton (string name, string resourceLoc, Vector3 location) {
		GameObject buttonGO = Instantiate(Resources.Load("SpriteButton")) as GameObject;
		buttonGO.transform.parent = this.transform;
		buttonGO.name = name;
		buttonGO.transform.localPosition = location;
		SpriteButton buttonScript = buttonGO.GetComponent<SpriteButton>();
		buttonScript.addListener(this);
		SpriteRenderer renderer = buttonGO.GetComponent<SpriteRenderer>();
		renderer.sprite = Resources.Load <Sprite> (resourceLoc);

		return buttonScript;
	}
	void initButtons() {
		passButtonScript = createButton("Passbutton", "buttons/" + "next", new Vector3(10f, 1f, 1f));
		diamondsButtonScript = createButton("diamondsbutton", "buttons/" + "diamonds", new Vector3(10f, 2f, 1f));
		clubsButtonScript = createButton("cslubsbutton", "buttons/" + "clubs", new Vector3(10f, 3f, 1f));
		heartsButtonScript = createButton("heartsutton", "buttons/" + "hearts", new Vector3(10f, 4f, 1f));
		spadesButtonScript = createButton("spadesbutton", "buttons/" + "spades", new Vector3(10f, 5f, 1f));
		noTrumpButtonScript = createButton("notrumpbutton", "buttons/" + "notrump", new Vector3(10f, 6f, 1f));

		firstCardButtonScript = createButton("firstcardbutton", "buttons/" + "first", new Vector3(17f, 7f, 1f));
		secondCardButtonScript = createButton("secondcardbutton", "buttons/" + "2", new Vector3(17f, 8f, 1f));


		float i = 0f;
		foreach (KeyValuePair<Rank, string> kvp in GlobalMembers.RANK_STRING_MAPPING) {
			SpriteButton button = createButton($"{kvp.Value}button", "buttons/" + kvp.Value, new Vector3(19f, i,1f));
			i = i + 1f;
			rankDictMappingScripts[button] = kvp.Key;
		}
	}

	public void buttonClicked(SpriteButton scr) {
		Logger.logMessage("BUTTON CLCIKED");

		Player player = model.getHumanPlayer(); 
		List<Card> selectedCards = handViewScript.getSelectedCards();
		if (scr == passButtonScript) {
			switch(model.getState()) {
				case GameState.PRE_DEAL:
					model.newRound();
					break;
				case GameState.END:
					model.newGame(Constants.NUM_PLAYERS);
					break;
				case GameState.DEAL:
					model.receiveBid(player, Suit.SUIT_INVALID);
					break;
				case GameState.KITTY: 
					model.receiveBury(selectedCards);
					break;
				case GameState.PARTNER:
					model.receivePartner(partnerCard, partnerNth);
					break;
				case GameState.PLAY:
					model.receivePlay(player, selectedCards);
					break;	
				default:
					Logger.logMessage("ehh");
					break;
			}
		} else if (scr == diamondsButtonScript) {
			suitButtonClicked(Suit.DIAMONDS);
		} else if (scr == clubsButtonScript) {
			suitButtonClicked(Suit.CLUBS);
		} else if (scr == heartsButtonScript) {
			suitButtonClicked(Suit.HEARTS);
		} else if (scr == spadesButtonScript) {
			suitButtonClicked(Suit.SPADES);
		} else if (scr == noTrumpButtonScript) {
			suitButtonClicked(Suit.NO_TRUMP);
		} else if (rankDictMappingScripts.ContainsKey(scr)) {
			rankButtonClicked(rankDictMappingScripts[scr]);
		} else if (scr == firstCardButtonScript) {
			partnerNth = 1;
			updateMiscTextView();
		} else if (scr == secondCardButtonScript) {
			partnerNth = 2;
			updateMiscTextView();
		}
	}
	private void rankButtonClicked(Rank r) {
		if (model.getState() == GameState.PARTNER) {
			partnerRank = r;
			partnerCard = new Card(partnerRank, partnerSuit);
			updateMiscTextView();
		}
	}

	private void suitButtonClicked(Suit s) {
		if (model.getState() == GameState.PARTNER) {
			if (s == Suit.NO_TRUMP) {
				partnerCard = null;
			} else {
				partnerSuit = s;
				partnerCard = new Card(partnerRank, partnerSuit);
				updateMiscTextView();
			}
		} else if (model.getState() == GameState.DEAL) {
			Player player = model.getHumanPlayer(); 
			model.receiveBid(player, s);
		}
	}


//TODO UPDATE stuff to use
	private void updatePlayerViews() {
		int i = 0;
		foreach (PlayerDetailsView view in playerDetailsViewScripts) {
			view.setPlayer(model.getPlayer(i));
			i++;
		}
	}

	private void updateGameDetailsView() {
		gameDetailsScript.updateGame(model);
	}

	private void updateMiscTextView() {
		if (model.getState() == GameState.PARTNER) {
			miscScript.update(partnerCard, partnerNth);
		} else {
			miscScript.update();
		}
	}

	void Update () {
		if (Input.GetKeyDown(KeyCode.Space)) {
            Player player = model.getHumanPlayer(); 
			model.receiveBid(player, Suit.SUIT_INVALID);
        }
	}

	public void bidUpdated() {
		logMessage("bid updated");
	}
	public void updateAllPlayers() {
		logMessage("updateAllPlayers");

	}
	public void updateRoundDetails() {
		//logMessage("updateRoundDetailss");

	}
	public void playerRequestBid() {
		//logMessage("requestbid");

	}
	public void playerRequestMove() {
		//logMessage("requestmove");

	}

	public void trickBegin() {
		//logMessage("trick begin");

	}
	public void eventOccurred(string s) {
//		logMessage($" eventOccurred: {s}");

		// if (s == GameEvents.HAND_UPDATE) {
			List<Card> cards = model.getHumanPlayer().getHand();
			handViewScript.setCards(cards);
		// }

		updateBoardViews();
		updateMiscTextView();
		updatePlayerViews();
		updateGameDetailsView();

	}

	void updateBoardViews() {
		Trick trick = model.getCurrentTrick();

		for (int i = 0; i < Constants.NUM_PLAYERS; i++) {
			if (model.getState() != GameState.PLAY) {
				boardGameViewScripts[i].setCards(new List<Card>());
			} else {
				List<Card> play = null;
				if (trick != null) {
					play = trick.getPlay(i);
				}
				if (play == null) {
					play = new List<Card>();
				}
				boardGameViewScripts[i].setCards(play);
			}
		}
	}
}
