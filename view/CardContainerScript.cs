using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class CardContainerScript : MonoBehaviour {

	protected List<ICardContainerListener> listeners = new List<ICardContainerListener>();
	protected List<SpriteRenderer> savedSprites = new List<SpriteRenderer>();

	protected List<Card> cards;
	protected List<GameObject> views;
	protected List<CardViewScript> viewsScripts;

	public void addCardContainerListener(ICardContainerListener listener)
	{
		listeners.Add(listener);
	}

	public void removeListener(ICardContainerListener listener)
	{
		listeners.Remove(listener);
	}

	public virtual void setSortingLayerID(int id)
	{
		this.transform.GetComponent<Renderer>().sortingLayerID = id;

		MeshRenderer[] renders =  gameObject.GetComponentsInChildren<MeshRenderer> ();

		foreach (MeshRenderer render in renders)
		{
			render.sortingLayerID = id;
			render.sortingOrder = 50;
		}
		
		foreach (SpriteRenderer obj in savedSprites)
		{
			obj.sortingLayerID = id;
		}

	}
}
