﻿// using System;
// using System.Collections.Generic;
// class ConsoleMainVC : LoggerListener, ModelSub {
//     Game model;
//     static ConsoleMainVC vc;
//     static void Main(string[] args) {
//         vc = new ConsoleMainVC();

//         Console.WriteLine("Hello World!");
//         Logger.subscribe(vc);
//         vc.model = new Game();
//         vc.model.addSubscriber(vc);
//         vc.model.newGame(Constants.NUM_PLAYERS);

//         System.ConsoleKey key = Console.ReadKey().Key;
//         while (key != ConsoleKey.Escape) {
//             Player player = vc.model.getCurrentPlayer(); // TODO NEEDS TO BE CURRENT PLAYER
//             if (key == ConsoleKey.Spacebar) {
//                 vc.model.receiveBid(player);
//             } else if (key == ConsoleKey.D) {
//                 vc.model.receiveBid(player, Suit.DIAMONDS);
//             } else if (key == ConsoleKey.C) {
//                 vc.model.receiveBid(player, Suit.CLUBS);
//             } else if (key == ConsoleKey.H) {
//                 vc.model.receiveBid(player, Suit.HEARTS);
//             } else if (key == ConsoleKey.S) {
//                 vc.model.receiveBid(player, Suit.SPADES);
//             } else if (key == ConsoleKey.N) {
//                 vc.model.receiveBid(player, Suit.NO_TRUMP);
//             } else if (key == ConsoleKey.R) {
//                 vc.model.newRound();
//             } else if (key == ConsoleKey.B) {
//                 vc.logMessage("multi input bury");
//                 string inputCards = Console.ReadLine();
//                 try {
//                     List<Card> cards = parseCards(inputCards);
//                     vc.model.receiveBury(cards);
//                 }catch (Exception e) {
//                     vc.logMessage(e.ToString());
//                 }
//             } else if (key == ConsoleKey.P) {
//                 vc.logMessage("multi input partner");
//                 string input = Console.ReadLine();
//                 string[] inputs = input.Split(" ");
//                 try {
//                     List<Card> cards = parseCards(inputs[0]);
//                     int nth = int.Parse(inputs[1]);
//                     vc.model.receivePartner(cards[0], nth);

//                 } catch (Exception e) {
//                     vc.logMessage(e.ToString());
//                 }
//             } else if (key == ConsoleKey.G) {
//                 vc.logMessage("multi input play");
//                 try {
//                     string inputCards = Console.ReadLine();
//                     List<Card> cards = parseCards(inputCards);
//                     vc.model.receivePlay(player, cards);
//                 } catch (Exception e) {
//                     vc.logMessage(e.ToString());
//                 }
//             }
//             key = Console.ReadKey().Key;
//         }
//         vc.logMessage("WDF");
//     }

//     public static List<Card> parseCards(string s) {
//         String[] cardStrings = s.Split(" ");
//         List<Card> cards = new List<Card>();
//         foreach (string rankSuit in cardStrings) {
//             string rankString = rankSuit.Substring(0, 1);
//             string suitString = rankSuit.Substring(1, 1);
//             Card card = new Card(GlobalMembers.STRING_RANK_MAPPING[rankString], GlobalMembers.SIMPLE_SUIT_STRING_SUIT_MAPPING[suitString]);
//             cards.Add(card);
//         }
//         return cards;
//     }

//     public void logMessage(string s) {
//         Console.WriteLine(s);
//     }

//     public void bidUpdated() {
//         logMessage("bid updated");
//     }
//     public void updateAllPlayers() {
//         logMessage("updateAllPlayers");

//     }
//     public void updateRoundDetails() {
//         logMessage("updateRoundDetailss");

//     }
//     public void playerRequestBid() {
//         logMessage("requestbid");

//     }
//     public void playerRequestMove() {
//         logMessage("requestmove");

//     }

//     public void trickBegin() {
//         logMessage("trick begin");

//     }
//     public void eventOccurred(string s) {
//         logMessage($" eventOccurred: {s}");

//     }
// }
