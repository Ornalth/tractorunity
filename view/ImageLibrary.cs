﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class ImageLibrary {

	static readonly string CARD_BASE_LOC = "png/";
	static Dictionary <Card, Sprite> cardLibrary = null;
	
	public static Sprite getCardImage(Card c) {
		if (cardLibrary == null) {
			getCardLibraryDict();
		}
//		Logger.logMessage($"{c}");
		return cardLibrary[c];
	}

//TODO
	private static void getCardLibraryDict()
	{
		cardLibrary = new Dictionary<Card,Sprite>();
		foreach(KeyValuePair<Rank, string> rankKVP in GlobalMembers.RANK_STRING_MAPPING)
		{
			foreach(KeyValuePair<Suit, string> suitKVP in GlobalMembers.SUIT_STRING_MAPPING)
			{
				Rank rank = rankKVP.Key;
				Suit suit = suitKVP.Key;
				if (suit == Suit.JOKER || rank == Rank.JOKER_UNC || rank == Rank.JOKER_COL) {
					continue;
				}

				Card card = new Card(rank, suit);
				string cardString = rankKVP.Value.ToLower() + "of" + suitKVP.Value.ToLower();

				Sprite spr = Resources.Load <Sprite> (CARD_BASE_LOC + cardString);
				if (spr != null) {
					cardLibrary[card] = spr; 
				} else {
					Logger.logMessage("OMFG NO SPRITE NAMED " + CARD_BASE_LOC+ cardString);
				}
			}
		}

		string jokerstring = GlobalMembers.RANK_STRING_MAPPING[Rank.JOKER_UNC].ToLower() + "joker";
		Sprite spriteJoker = Resources.Load <Sprite> (CARD_BASE_LOC + jokerstring);
		if (spriteJoker != null) {
			cardLibrary[CardUtils.generateJoker(Rank.JOKER_UNC)] = spriteJoker; 
		} else {
			Logger.logMessage("OMFG NO SPRITE NAMED " + CARD_BASE_LOC+ jokerstring);
		}

		jokerstring = GlobalMembers.RANK_STRING_MAPPING[Rank.JOKER_COL].ToLower() + "joker";
		spriteJoker = Resources.Load <Sprite> (CARD_BASE_LOC + jokerstring);
		if (spriteJoker != null) {
			cardLibrary[CardUtils.generateJoker(Rank.JOKER_COL)] = spriteJoker; 
		} else {
			Logger.logMessage("OMFG NO SPRITE NAMED " + CARD_BASE_LOC+ jokerstring);
		}
	}
}
