using UnityEngine;
using System.Collections.Generic;
using TMPro;

public class PlayerDetailsView : MonoBehaviour {

    TextMeshPro  textBox;

	void Awake() {
		textBox = gameObject.GetComponentInChildren<TextMeshPro >();
	}

	public void setPlayer(Player p) {
		string output = genPlayerString(p);
        textBox.text = output;
	}
    private string genPlayerString(Player p) {
        string[] wat = new string[] {
			$"Name:{p.getName()}",
			$"Points:{p.getPoints()}",
			$"level:{p.getCurrentLevel()}",
			$"Player Type:{p.getCurrentRoundPlayerType()}"
		};
        return string.Join("\n", wat);  
    }
}
