﻿using System.Collections;
using System.Collections.Generic;

public interface ModelSub {
    void bidUpdated();
    void updateAllPlayers();
    void updateRoundDetails();
    void playerRequestBid();
    void playerRequestMove();
    void logMessage(string message);

    void trickBegin();
    void eventOccurred(string s);
}